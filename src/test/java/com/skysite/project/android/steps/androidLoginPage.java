package com.skysite.project.android.steps;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.waitForPresent;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class androidLoginPage{
	
	public void login(String userName, String Password) {
		if (new QAFExtendedWebElement("project.update.btn").isPresent()){
			click("project.update.btn");
		}
		if (new QAFExtendedWebElement("project.menu.btn").isPresent()){
			logOut();
		}
		else if (new QAFExtendedWebElement("banner.skip.button").isPresent()){
			click("banner.skip.button");
			logOut();
		}
		System.out.println("++++++++++Username is "+userName);
		System.out.println("++++++++++passsword is "+Password);
		click("login.username.txtbox");
		sendKeys(userName, "login.username.txtbox");
		click("login.password.txtbox");
		sendKeys(Password, "login.password.txtbox");
		click("login.login.btn");
		if (new QAFExtendedWebElement("edirpro.header.txt").isPresent()){
			click("editpro.save.btn");
		}
		
	}
	
	public  void logOut(){
		QAFTestBase.pause(2000);
		click("project.menu.btn");
		waitForPresent("logout.logout.btn");
		click("logout.logout.btn");
		waitForPresent("project.signout.ok.btn");
		click("project.signout.ok.btn");
		
		if (new QAFExtendedWebElement("banner.ok.button").isPresent()) {
			click("banner.ok.button");
		}
		
	}

}
