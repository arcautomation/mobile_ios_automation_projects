package com.skysite.project.util;

import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration.ConfigurationUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.internal.ConfigurationGroupMethods;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.skysite.project.ios.steps.loginPage;
import com.skysite.project.web.addFolder;
import com.skysite.project.web.webLoginPage;

public class testNgListner implements ITestListener {
	
	public static WebDriver driver;
	
	public static addFolder addFolder;
	public static webLoginPage loginPage;

	@Override
	public void onFinish(ITestContext arg0) {
		driver.close();
		driver.quit();

	}

	@Override
	public void onStart(ITestContext arg0) {
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		addFolder = PageFactory.initElements(driver, addFolder.class);
		loginPage = PageFactory.initElements(driver, webLoginPage.class);

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestFailure(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestSkipped(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestStart(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestSuccess(ITestResult arg0) {
		// TODO Auto-generated method stub

	}
}
