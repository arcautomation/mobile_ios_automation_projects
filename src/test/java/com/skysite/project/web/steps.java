package com.skysite.project.web;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Reporter;
import com.skysite.project.ios.steps.common;
import com.skysite.project.ios.steps.loginPage;
import com.skysite.project.util.testNgListner;
import com.skysite.project.web.addFolder;

public class steps {

	static WebDriver driver;

	
	
	public void beforeMethod() {
		

	}

	@QAFTestStep(description = "User opne the web browser")
	public void openBrowser() {
		testNgListner.driver.get("https://stg.skysite.com/app");
		Reporter.log("browser open");

	}
	@QAFTestStep(description = "User logs in to the web application")
	public void doLogin(){
		testNgListner.loginPage.webLogin();
	}

	@QAFTestStep(description = "User create a new folder")
	public void webAddFolder() {
		testNgListner.addFolder.webOpenProject();
		testNgListner.addFolder.New_Folder_Create();
	}
}
