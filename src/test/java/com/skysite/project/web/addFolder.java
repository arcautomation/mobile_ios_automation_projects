package com.skysite.project.web;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Reporter;
import com.skysite.project.ios.steps.common;

public class addFolder {
	@FindBy(css = "#aPrjAddFolder")
	static WebElement btnAddNewFolder;

	@FindBy(css = "#txtFolderName")
	static WebElement txtBoxFolderName;

	@FindBy(css = "#btnFolderCreate")
	static WebElement btnCreateFolder;

	@FindBy(css = ".noty_text")
	static WebElement notificationMsg;
	
	@FindBy(xpath="//*[contains(@id,'PNumber_')]")
	WebElement txtProjectName;

	final WebDriver driver;

	public addFolder(WebDriver driver)

	{
		this.driver = driver;
	}
	public void webOpenProject(){
		txtProjectName.click();
	}

	public  void New_Folder_Create() {
		QAFTestBase.pause(5000);
		;
		Reporter.log("Waiting for Create Folder button to be appeared");
		QAFTestBase.pause(3000);
		btnAddNewFolder.click();// Clicking on Create New Folder
		Reporter.log("Add New folder button clicked!!!");
		QAFTestBase.pause(5000);
		QAFTestBase.pause(5000);
		String Foldername = "New Folder " + common.getRandomValue();
		ConfigurationManager.getBundle().setProperty("webFoldername", Foldername);
		txtBoxFolderName.sendKeys(Foldername);// Giving Folder Names Randomly
		Reporter.log("Folder Name: " + Foldername + " has been entered in Folder Name text box.");
		btnCreateFolder.click();
		Reporter.log("Create Folder button clicked!!!");// .noty_text
		QAFTestBase.pause(5000);
		String Msg_Folder_Create = notificationMsg.getText();
		Reporter.log("Notification Message after folder create is: " + Msg_Folder_Create);
		QAFTestBase.pause(5000);
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Reporter.log("Available Folder Count is: " + Avl_Fold_Count);

		for (int j = 1; j <= Avl_Fold_Count; j++) {
			actualFolderName = driver
					.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
					.getText();
			Reporter.log("Exp Name:" + Foldername);
			Reporter.log("Act Name:" + actualFolderName);

			// Validating the new Folder is created or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) {
				Reporter.log("Folder is created successfully with name: " + Foldername);
				break;
			}
		}

	}

}
