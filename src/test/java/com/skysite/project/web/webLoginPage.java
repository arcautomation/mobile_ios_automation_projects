package com.skysite.project.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Reporter;
import com.skysite.project.util.testNgListner;
import com.thoughtworks.selenium.webdriven.commands.Click;

public class webLoginPage {

	@FindBy(css = "#UserID")
	WebElement txtBoxUserName;

	@FindBy(css = "#Password")
	static WebElement txtBoxPassword;

	@FindBy(css = "#btnLogin")
	static WebElement btnLogin;

	@FindBy(xpath = "//*[@id='WhatsNewFeature']/div/div/div[3]/button[contains(text(),'Got it')]")
	static WebElement btnGotItBanner;
	
	@FindBy(xpath = "//*[@id='btnlater']/span[contains(text(),'Ask me later')]")
	static WebElement askMeLater;
	
	final WebDriver driver;

	public webLoginPage(WebDriver driver)

	{

		this.driver = driver;

	}

	
	public void webLogin() {
		
		txtBoxUserName.sendKeys("automation@mailinator.com");
		txtBoxPassword.sendKeys("arcind@123");
		btnLogin.click();
		Reporter.log("Clicked on Login");
		if (btnGotItBanner.isDisplayed()) {
			btnGotItBanner.click();
			Reporter.log("Skipped instruction");
		}
		if(driver.findElements(By.xpath("//*[@id='btnlater']/span[contains(text(),'Ask me later')]")).size() >0){
			askMeLater.click();
			Reporter.log("Skipped Feedback");
		}

	}

}
