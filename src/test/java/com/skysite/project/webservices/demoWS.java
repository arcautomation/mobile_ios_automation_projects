package com.skysite.project.webservices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

public class demoWS {
	public static String baseURI = "https://stg.skysite.com/skysiteapi/api";

	public static String getAuthKey(String userName, String password, String wsresponce, String key) {
		RestAssured.baseURI = baseURI;
		RequestSpecification httpRequest = RestAssured.given();
		HashMap<String, String> header = new HashMap<String, String>();
		header.put("LoginId", userName);
		header.put("Password", password);
		header.put("Content-Type", "application/json");
		header.put("ModuleIdKeyRequest", "PWP");
		header.put("Decrypt", "false");
		header.put("Decrypt", "false");

		Response response = httpRequest.headers(header).post("/ARCAuthentication/SignIn");
		String keyValue=null;
		if (wsresponce.contains("header")) {
			keyValue = response.getHeaders().get(key).getValue().toString();
		}else{
			keyValue = response.body().jsonPath().getString(key);
		}

		System.out.println("Response Body is =>  " + keyValue);
		return keyValue;

	}

	public static void getContactList() throws IOException {
		String authKey = getAuthKey("Automation@mailinator.com", "arcind@123","header","tokenkey");
//		
		HashMap<String, String> header = new HashMap<String, String>();
		header.put("TokenKey",authKey);
		header.put("ModuleKeyRequest","PWP");
		header.put("Accept-Language","en-US");
		header.put("Accept-Encoding","gzip");
		header.put("Content-Type","application/json");
		
		HashMap< String, String> parametersMap = new HashMap<>();
		parametersMap.put("mode","0");
		parametersMap.put("externalUser","0");
		parametersMap.put("startIndex","0");
		parametersMap.put("endIndex","0");	
		
		RestAssured.baseURI = baseURI;
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.headers(header).queryParams(parametersMap).get("/Contact");
		List<String> id = new ArrayList<>();
		id = response.body().jsonPath().getList("PWContactID");
		System.out.println(id);
		String accid = getAuthKey("Automation@mailinator.com", "arcind@123","body","PWContactID");
		System.out.println(accid);
		
	}

	public static void main(String[] args) throws IOException {
		System.out.println("=============");
		// getAuthKey("Automation@mailinator.com", "arcind@123");
		getContactList();
	}

}
