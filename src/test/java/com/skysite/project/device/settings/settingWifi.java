package com.skysite.project.device.settings;

import static com.qmetry.qaf.automation.step.CommonStep.click;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebDriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.TouchShortcuts;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class settingWifi {

	static IOSDriver driver = null;

	@QAFTestStep(description = "User open device settings")
	public static void openSetting() throws MalformedURLException {

		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
		capabilities.setCapability("bundleId", "com.apple.Preferences");
		capabilities.setCapability("platformName", "iOS");
		capabilities.setCapability("udid", "725d4b10448b9851578a2cfe45880813f71732ce");
		capabilities.setCapability("fullReset", "false");
		capabilities.setCapability("noReset", "true");
		capabilities.setCapability("deviceName", "iPad Air");
		driver = new IOSDriver(new URL("http://10.10.86.147:4723/wd/hub"), capabilities);
		System.out.println("Driver created");
		driver.swipe(162, 330, 184, 666, 1000);
	}

	@QAFTestStep(description = "User opens application again")
	public static void openAppAgain() throws MalformedURLException {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
		// capabilities.setCapability("bundleId", "com.apple.Preferences");
		capabilities.setCapability("platformName", "iOS");
		capabilities.setCapability("udid", "725d4b10448b9851578a2cfe45880813f71732ce");
		capabilities.setCapability("app", "/Users/subhendudas/Documents/Mobile_Automation/app/SKYSITE.ipa");
		capabilities.setCapability("fullReset", "false");
		capabilities.setCapability("noReset", "true");
		capabilities.setCapability("deviceName", "iPad Air");
		IOSDriver driver = new IOSDriver(new URL("http://10.10.86.147:4723/wd/hub"), capabilities);
		System.out.println("Driver created");
		QAFTestBase.pause(10000);
		;
		if (new QAFExtendedWebElement("banner.ok.button").isPresent()) {
			click("banner.ok.button");
		}
	}

	@QAFTestStep(description = "User enable the wifi")
	public static void enableWifi() {
		QAFTestBase.pause(2000);
		driver.findElement(By.name("Wi-Fi")).click();
		if (driver.findElement(By.xpath("//XCUIElementTypeSwitch[@name='Wi-Fi']")).getAttribute("value").equals("0")) {
			driver.findElement(By.xpath("//XCUIElementTypeSwitch[@name='Wi-Fi']")).click();
			Reporter.log("Enabled wifi", MessageTypes.Info);
		}

	}

	@QAFTestStep(description = "User disable the wifi")
	public static void disableWifi() {
		driver.findElement(By.name("Wi-Fi")).click();
		if (driver.findElement(By.xpath("//XCUIElementTypeSwitch[@name='Wi-Fi']")).getAttribute("value").equals("1")) {
			driver.findElement(By.xpath("//XCUIElementTypeSwitch[@name='Wi-Fi']")).click();
			Reporter.log("disabled wifi", MessageTypes.Info);
		}
	}

	public static void swipeToOpenWifi() {
		Dimension size = new WebDriverTestBase().getDriver().manage().window().getSize();
		int startx = size.getWidth() / 2;
		int starty = size.getHeight() - 10;
		int endy = 50;
		 System.out.println("=============="+startx);
		 System.out.println("=============="+starty);
		 System.out.println("=============="+endy);
		// (new WebDriverTestBase().getDriver()).swipe(startx, starty, startx,
		// endy, 1000);

		TouchAction action = new TouchAction((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
		action.press(startx, starty).waitAction(1000).moveTo(startx, endy).release().perform();
		// QAFTestBase.pause(10000);

	}
	
	public static void swipeToOPenApp(){
		Dimension size = new WebDriverTestBase().getDriver().manage().window().getSize();
		int startx = size.getWidth() / 2;
		int starty = 0;
		int endy = size.getHeight();
		// System.out.println("=============="+startx);
		// System.out.println("=============="+starty);
		// System.out.println("=============="+endy);
		// (new WebDriverTestBase().getDriver()).swipe(startx, starty, startx,
		// endy, 1000);

		TouchAction action = new TouchAction((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
		action.press(startx, starty).waitAction(1000).moveTo(startx, endy).release().perform();
		
	}

	@QAFTestStep(description = "User swipes to disable the wifi")
	public static void wifiDisable() {
		// swipe("150", "150", 5);
		swipeToOpenWifi();
		if (new QAFExtendedWebElement("dv.wifi.btn").getAttribute("value").equals("1")) {
			click("dv.wifi.btn");
		}
		swipeToOPenApp();
		swipeToOpenWifi();
//		click("dv.app");

	}
	
	@QAFTestStep(description = "User swipes to enable the wifi")
	public static void wifiENable(){
		swipeToOpenWifi();
		QAFTestBase.pause(5000);
		if (new QAFExtendedWebElement("dv.wifi.btn").getAttribute("value").equals("0")) {
			click("dv.wifi.btn");
		}
		swipeToOPenApp();
		swipeToOpenWifi();
//		click("dv.app");
	}

	public static void swipe(String start, String end, int duration) {
		String command = "mobile:swipe";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("start", start);
		params.put("end", end);
		params.put("duration", duration);
		new WebDriverTestBase().getDriver().executeScript(command, params);
	}

}
