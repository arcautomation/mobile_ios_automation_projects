package com.skysite.project.ios.steps;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForPresent;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class loginPage {
	
	@QAFTestStep(description = "User log in with {username} and {password}")
	public static void login(String userName, String Password) {
		if (new QAFExtendedWebElement("project.update.btn").isPresent()){
			click("project.update.btn");
		}
		QAFTestBase.pause(2000);
		if (new QAFExtendedWebElement("project.menu.btn").isPresent()){
			logOut();
		}
		else if (new QAFExtendedWebElement("banner.skip.button").isPresent()){
			click("banner.skip.button");
			logOut();
		}
		System.out.println("++++++++++Username is "+userName);
		System.out.println("++++++++++passsword is "+Password);
		click("login.username.txtbox");
		sendKeys(userName, "login.username.txtbox");
		click("login.password.txtbox");
		sendKeys(Password, "login.password.txtbox");
		click("login.login.btn");
		if (new QAFExtendedWebElement("edirpro.header.txt").isPresent()){
			click("editpro.save.btn");
		}
		
	}
	
	@QAFTestStep(description="User log out from the application")
	public static void logOut(){
		QAFTestBase.pause(2000);
		click("project.menu.btn");
		waitForPresent("logout.logout.btn");
		click("logout.logout.btn");
		waitForPresent("project.signout.ok.btn");
		click("project.signout.ok.btn");
		waitForNotPresent("project.loader.icon", 180);
		if (new QAFExtendedWebElement("banner.ok.button").isPresent()) {
			click("banner.ok.button");
		}
		waitForNotPresent("project.loader.icon", 180);
		
	}
	
}
