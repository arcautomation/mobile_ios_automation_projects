package com.skysite.project.ios.steps;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;
import static com.qmetry.qaf.automation.step.CommonStep.*;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;

public class folder {

	@QAFTestStep(description = "User slide the knob to unlink the file from cloud")
	public static void unLinkFile() {
		click("folder.file.cloud.knob");
		QAFExtendedWebElement ele = new QAFExtendedWebElement("folder.sync.ok.btn");
		if (ele.isPresent() && ele.getAttribute("label").equals("Ok")) {
			click("folder.sync.ok.btn");

		} else {
			waitForNotPresent("project.loader.icon", 180);
			waitForPresent("folder.file.cloud.knob");
			click("folder.file.cloud.knob");
			click("folder.sync.ok.btn");
		}
	}

	@QAFTestStep(description = "User select the {foldername} folder to download")
	public static void folderDownload(String folderName) {

		QAFTestBase.pause(2000);
		if (new QAFExtendedWebElement("project.photo.popup.ok.btn").isPresent()) {
			click("project.photo.popup.ok.btn");
		}
		waitForPresent("project.navclose.btn");

		click("project.navclose.btn");
		QAFTestBase.pause(2000);
		TouchAction action = new TouchAction((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
		action.longPress(
				new QAFExtendedWebElement(
						(String.format(getBundle().getString("project.folder.name.txt"), folderName, folderName))),
				3000).perform().release();

		click("folder.download.btn");
	}

	@QAFTestStep(description = "User should see viwerpage")
	public static void verifyDownloadedFile() {
		waitForNotPresent("project.loader.icon", 180);
		verifyPresent("project.photo.drawer");
		verifyPresent("rfi.drawer.btn");
	}

	@QAFTestStep(description = "User open {folderName} folder")
	public static void openFolder(String folderName) {
		QAFTestBase.pause(2000);
		new QAFExtendedWebElement(
				String.format(getBundle().getString("project.folder.name.txt"), folderName, folderName)).click();

	}

	public static void preMoveFolder() {
		if (!new QAFExtendedWebElement("folder.movr.txt").isDisplayed()) {

			String folder = ConfigurationManager.getBundle().getProperty("moveTo").toString();
			String folderName = ConfigurationManager.getBundle().getProperty("folderMove").toString();

			new QAFExtendedWebElement(String.format(getBundle().getString("project.folder.name.txt"), folder, folder))
					.click();
			click("folder.more.btn");
			click("folder.more.select.btn");

			QAFTestBase.pause(2000);
			new QAFExtendedWebElement(
					String.format(getBundle().getString("project.folder.name.txt"), folderName, folderName)).click();
			click("folder.more.btn");
			click("folder.more.move.btn");

			click("folder.move.btn");
			click("folder.move.conf.btn");

			click("folder.brdcrmb.navigation");
			click("folder.root.view");
		}
	}

	@QAFTestStep(description = "User select {folderName} folder and move to {folder}")
	public static void selectFolderToMove(String folderName, String folder) {

//		preMoveFolder();
		click("folder.more.btn");
		click("folder.more.select.btn");

		QAFTestBase.pause(2000);
		new QAFExtendedWebElement(
				String.format(getBundle().getString("project.folder.name.txt"), folderName, folderName)).click();
		click("folder.more.btn");
		click("folder.more.move.btn");

		QAFTestBase.pause(2000);
		new QAFExtendedWebElement(String.format(getBundle().getString("project.folder.name.txt"), folder, folder))
				.click();
		click("folder.move.btn");
//		click("folder.move.conf.btn");

	}

	@QAFTestStep(description = "User mover item to {folderName}")
	public static void movetoFolder(String folderName) {

		QAFTestBase.pause(2000);
		new QAFExtendedWebElement(
				String.format(getBundle().getString("project.folder.name.txt"), folderName, folderName)).click();
		click("folder.move.btn");
		click("folder.move.conf.btn");

	}

	@QAFTestStep(description = "User should see the moved folder")
	public static void verifyMovedFolder() {
		String folderName = ConfigurationManager.getBundle().getProperty("folderMove").toString();
		new QAFExtendedWebElement(
				String.format(getBundle().getString("project.folder.name.txt"), folderName, folderName))
						.verifyPresent();

	}

}
