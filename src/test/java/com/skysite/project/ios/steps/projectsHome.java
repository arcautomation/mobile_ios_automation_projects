package com.skysite.project.ios.steps;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;
import static com.qmetry.qaf.automation.step.CommonStep.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.testng.report.Report;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.MultiTouchAction;
import io.appium.java_client.TouchAction;

public class projectsHome {

	@QAFTestStep(description = "User should see the project screen")
	public static void projectScreenVerification() {
		verifyPresent("project.search.icon");
		verifyPresent("project.search.btn");
		verifyPresent("project.header.txt");

	}

	@QAFTestStep(description = "User should see only the shared project")
	public static void sharedProjectVerification() {
		click("project.shared.btn");
		verifyPresent("project.iteam.list");
	}

	@QAFTestStep(description = "User select any project as favorites")
	public static void markProjectAsFav() {
		click("project.fav.btn");
		if (new QAFExtendedWebElement("project.fav.icon.list").isPresent()) {
			List<QAFWebElement> favIconListInFav = new WebDriverTestBase().getDriver()
					.findElements("project.fav.icon.list");
			for (int j = 0; j < favIconListInFav.size(); j++) {
				favIconListInFav.get(j).click();
			}
		}
		click("project.myproject.btn");
		List<QAFWebElement> favIconList = new WebDriverTestBase().getDriver().findElements("project.fav.icon.list");
		List<QAFWebElement> projectNameList = new WebDriverTestBase().getDriver().findElements("project.name.txt.list");

		favIconList.get(0).click();

		ConfigurationManager.getBundle().setProperty("proName", projectNameList.get(0).getAttribute("label"));
		System.out.println(ConfigurationManager.getBundle().getProperty("proName"));
	}

	@QAFTestStep(description = "User unselct any project as favorite")
	public static void markProjectAsUnFav() {
		click("project.fav.btn");
		if (new QAFExtendedWebElement("project.fav.icon.list").isPresent()) {
			List<QAFWebElement> favIconListInFav = new WebDriverTestBase().getDriver()
					.findElements("project.fav.icon.list");
			for (int j = 0; j < favIconListInFav.size(); j++) {
				favIconListInFav.get(j).click();
			}
		}
	}

	@QAFTestStep(description = "User should see the project inside the favorite project list")
	public static void verifyFavProject() {
		click("project.fav.btn");
		List<QAFWebElement> projectNameList = new WebDriverTestBase().getDriver().findElements("project.name.txt.list");
		List<String> name = new ArrayList<>();
		for (int i = 0; i < projectNameList.size(); i++) {
			System.out.println(projectNameList.size());
			System.out.println(projectNameList.get(i).getAttribute("value"));
			name.add(projectNameList.get(i).getAttribute("value").toString());
			System.out.println("done");
			System.out.println(name);
		}
		System.out.println(name);
		System.out.println(ConfigurationManager.getBundle().getProperty("proName"));
		Validator.verifyThat(name.contains(ConfigurationManager.getBundle().getProperty("proName")),
				Matchers.equalTo(true));

	}

	@QAFTestStep(description = "User should not see the project in the favorite list")
	public static void verifyFavProjectNotPresent() {
		click("project.fav.btn");
		Validator.verifyThat(new QAFExtendedWebElement("project.refresh.btn").isPresent(), Matchers.equalTo(true));

	}

	@QAFTestStep(description = "User slide the cloud knob and sync from thumbnail")
	public static void slideCloudKnob() {

		if (new QAFExtendedWebElement("project.sync.switch.lbl").getAttribute("value").contains("Device")) {
			click("project.pro.name");
			System.out.println("________________________"
					+ new QAFExtendedWebElement("project.sync.switch.lbl").getAttribute("value"));
			//
			// // String name = getText("project.cloud.btn");
			// System.out.println("name++++++++++++" + new
			// QAFExtendedWebElement("project.cloud.btn").getText());
			// click("project.pro.name");
			// click("project.sync.Thumbnels.option");
			// click("project.sync.sync.btn");
			//
		} else {
			click("project.pro.name");
			click("project.sync.sync.btn");
			System.out.println("________________________" + "in else cond");
		}

	}

	@QAFTestStep(description = "User unsync the project")
	public static void unSyncProject() {
		if (new QAFExtendedWebElement("project.sync.switch.lbl").getAttribute("label").contains("Device")) {
			click("project.cloud.knob");
			click("folder.sync.ok.btn");
		}
	}

	@QAFTestStep(description = "User should see the newly created folder")
	public static void verifyFolder() {
		String folderName = ConfigurationManager.getBundle().getString("webFoldername");
		System.out.println("====================================" + folderName + "================================");
		new QAFExtendedWebElement(
				String.format(getBundle().getString("project.folder.name.txt"), folderName, folderName)).click();
	}

	@QAFTestStep(description = "User select {folderName} folder")
	public static void selectFolder(String folderName) {
		QAFTestBase.pause(2000);
		if (new QAFExtendedWebElement("project.photo.popup.ok.btn").isPresent()) {
			click("project.photo.popup.ok.btn");
		}
		waitForPresent("project.navclose.btn");

		click("project.navclose.btn");

		// new QAFExtendedWebElement(
		// String.format(getBundle().getString("project.folder.name.txt"),
		// folderName, folderName)).click();
		new QAFExtendedWebElement(
				String.format(getBundle().getString("project.folder.name.txt"), folderName, folderName)).click();

	}

	@QAFTestStep(description = "User select {folderName} folder again")
	public static void selectSubFolder(String folderName) {
		// new QAFExtendedWebElement(
		// String.format(getBundle().getString("project.folder.name.txt"),
		// folderName, folderName)).click();
		new QAFExtendedWebElement(
				String.format(getBundle().getString("project.folder.name.txt"), folderName, folderName)).click();

	}

	@QAFTestStep(description = "User select {index} image")
	public static void selectImage(String index) {
		List<QAFWebElement> imageList = new WebDriverTestBase().getDriver().findElements("project.image.list");
		if (index.equals("any")) {
			imageList.get(0).click();
		} else {
			int ind = Integer.parseInt(index);
			imageList.get(ind).click();
		}
	}

	public static void selectDrawer(String drawer) {
		System.out.println("inside drawer");
		new QAFExtendedWebElement(String.format(getBundle().getString("project.drawer"), drawer, drawer)).click();
		// new
		// QAFExtendedWebElement(String.format(getBundle().getString("project.squere.drawerbox"),
		// drawer, drawer))
		// .click();

	}

	public static void selectSubDrawer(String subDrawer) {
		System.out.println("inside subdrawer");
		new QAFExtendedWebElement(
				String.format(getBundle().getString("project.subdrawer"), subDrawer + "Button", subDrawer + "Button"))
						.click();
	}

	public static void drawAnnotation() {
		TouchAction action = new TouchAction((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
		action.press(276, 577).moveTo(100, 100).release().perform();
	}

	public static void drawCalib() {
		TouchAction action = new TouchAction((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
		action.press(290, 600).moveTo(150, 150).release().perform();
	}

	@QAFTestStep(description = "User selects {drawer} drawer and draw {subdrawer} annotation")
	public static void draw(String drawer, String subDrawer) throws InterruptedException {

		switch (drawer) {
		case "square":
			selectDrawer("square");
			selectSubDrawer(subDrawer);
			drawAnnotation();
			break;
		case "line":
			selectDrawer("line");
			selectSubDrawer(subDrawer);
			drawAnnotation();
			break;
		case "highlight":
			selectDrawer("highlight");
			selectSubDrawer(subDrawer);
			drawAnnotation();
			break;
		case "freetext":
			selectDrawer("freetext");
			selectSubDrawer(subDrawer);
			drawAnnotation();
			if (subDrawer.contains("note")) {
				sendKeys("This is a Auto genereated message from Automation", "project.note.textbox");
				click("project.note.done.btn");
			} else {
				sendKeys("This is a Auto genereated message from Automation", "project.freetxt.editbox");
				click("project.done.btn");
			}
			break;
		case "Measurement Calibrator":
			selectDrawer("Measurement Calibrator");
			selectSubDrawer(subDrawer);
			drawAnnotation();
			break;
		default:
			break;
		}

	}

	@QAFTestStep(description = "User selects {drawer} drawer and draw {subdrawer} calibration")
	public static void drawCalibration(String drawer, String subDrawer) throws InterruptedException {
		selectDrawer("Measurement Calibrator");
		selectSubDrawer(subDrawer);
		drawCalib();
	}

	@QAFTestStep(description = "User save the annotation")
	public static void saveAnnotation() {
		click("project.save.btn");
		sendKeys("test", "project.savemarkup.texbox");
		click("project.savemarkup.save.btn");
		QAFTestBase.pause(5000);
	}

	@QAFTestStep(description = "User open the saved markups")
	public static String openMarkup() throws InterruptedException {
		click("project.mrkupanotation.btn");
		List<QAFWebElement> markupList = new WebDriverTestBase().getDriver().findElements("project.mrkup.list");
		if (new QAFExtendedWebElement("project.mrkupanotation.viewall.btn").isPresent()) {
			click("project.mrkupanotation.viewall.btn");
		} else {
			click("project.mrkupanotation.hideall.btn");
			click("project.mrkupanotation.btn");
			click("project.mrkupanotation.viewall.btn");
		}
		QAFTestBase.pause(5000);
		QAFExtendedWebElement ele = new QAFExtendedWebElement("project.image");
		String filename = ConfigurationManager.getBundle().getProperty("afterImage").toString();
		common.takeScrnShot(filename, ele);

		return filename;
	}

	@QAFTestStep(description = "User open the saved zoomed markups")
	public static String openZoomMarkup() throws InterruptedException {
		click("project.mrkupanotation.btn");
		List<QAFWebElement> markupList = new WebDriverTestBase().getDriver().findElements("project.mrkup.list");
		if (new QAFExtendedWebElement("project.mrkupanotation.viewall.btn").isPresent()) {
			click("project.mrkupanotation.viewall.btn");
		} else {
			click("project.mrkupanotation.hideall.btn");
			click("project.mrkupanotation.btn");
			click("project.mrkupanotation.viewall.btn");
		}
		QAFTestBase.pause(5000);
		QAFExtendedWebElement ele = new QAFExtendedWebElement("project.image");
		String filename = ConfigurationManager.getBundle().getProperty("afterImage").toString();
		common.takeScrnShotZoom(filename, ele);

		return filename;
	}

	@QAFTestStep(description = "User delete {number} saved markups")
	public static void deleteAllMarkup(String number) {

		click("project.mrkupanotation.btn");
		if (!new QAFExtendedWebElement("project.mrkupanotation.txt").isPresent()) {
			System.out.println("==================inside if=================");
			List<QAFWebElement> markupList = new WebDriverTestBase().getDriver().findElements("project.mrkup.list");
			int flag = markupList.size();
			if (number.equalsIgnoreCase("all")) {
				for (QAFWebElement ele : markupList) {
					System.out.println("=================================" + markupList);
					common.swipeLeftToDelete(ele);
					waitForPresent("project.mrkup.delete.btn");
					click("project.mrkup.delete.btn");
					waitForPresent("project.mrkup.delete.yes.btn");
					click("project.mrkup.delete.yes.btn");

					if (markupList.size() > 1 && flag > 0) {
						waitForPresent("project.mrkup.list");
					}
					if (markupList.size() == 1) {
						TouchAction action = new TouchAction(
								(MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
						action.tap(500, 200).perform();
					}
					flag = flag - 1;

				}
			} else {
				common.swipeLeftToDelete(markupList.get(Integer.parseInt(number)));
				System.out.println("==================inside else1=================");
			}
		} else {
			TouchAction action = new TouchAction(
					(MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
			action.tap(500, 200).perform();
			System.out.println("==================inside else2=================");
			System.out.println("++++++++++++++Touch with cordinate++++++++++++");
		}
	}

	@QAFTestStep(description = "User should see {bfrimg} and {aftrimg} same image")
	public static void compareImage(String beforImage, String afterImage) {
		int pass = 0;
		int fail = 0;
		try {
			BufferedImage imgA = ImageIO.read(new File(beforImage));
			BufferedImage imgB = ImageIO.read(new File(afterImage));
			if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {

				for (int x = 0; x < imgA.getWidth(); x++) {
					for (int y = 0; y < imgB.getHeight(); y++) {
						if (imgA.getRGB(x, y) == imgB.getRGB(x, y)) {
							pass = pass + 1;
							System.out.println("pass");
						} else {
							fail = fail + 1;
							System.out.println("fail");
						}

					}

				}
			}
			Reporter.log("Matched: " + pass + "/n" + "unmatched: " + fail);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (fail == 0) {
			Validator.verifyThat(true, Matchers.equalTo(true));
		} else {
			Validator.verifyThat(false, Matchers.equalTo(true));
		}
		File directory = new File("./screenCaptured");
		// try {
		// FileUtils.cleanDirectory(directory);
		// } catch (IOException e) {
		// Reporter.log("No file available inside the folder");
		// }

	}

	@QAFTestStep(description = "User removes the annotation")
	public static void removeAnnotation() {
		String drawer = "multi select";
		new QAFExtendedWebElement(String.format(getBundle().getString("project.drawer"), drawer, drawer)).click();
		TouchAction action = new TouchAction((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
		action.press(276, 577).moveTo(100, 100).release().perform();
		waitForPresent("project.annotation.delete.btn");
		click("project.annotation.delete.btn");
		String filename = ConfigurationManager.getBundle().getProperty("afterImage").toString();
		QAFExtendedWebElement ele = new QAFExtendedWebElement("project.image");
		common.takeScrnShot(filename, ele);
	}

	@QAFTestStep(description = "User take screenshot of the image")
	public static void saveScreenshot() {
		QAFTestBase.pause(2000);
		String fileName = ConfigurationManager.getBundle().getProperty("beforeImage").toString();
		QAFExtendedWebElement ele = new QAFExtendedWebElement("project.image");
		common.takeScrnShot(fileName, ele);
	}

	@QAFTestStep(description = "User take screenshot of the zoom image")
	public static void saveScreenshotZoom() {
		QAFTestBase.pause(2000);
		String fileName = ConfigurationManager.getBundle().getProperty("beforeImage").toString();
		QAFExtendedWebElement ele = new QAFExtendedWebElement("project.image");
		common.takeScrnShotZoom(fileName, ele);
	}

	@QAFTestStep(description = "User enter calibration value")
	public static void enterCalibration() {

		sendKeys("1", "project.calibration.length.txtbox1");
		click("project.calibration.done.btn");

	}

	@QAFTestStep(description = "User tap on the image and add photo")
	public static void addPhoto() {
		punchPage.tapOnImage();
		if (new QAFExtendedWebElement("punch.allow.btn").isPresent()) {
			click("punch.allow.btn");
		}
		common.captureImage();

	}

	@QAFTestStep(description = "User save photo")
	public static void savePhoto() {
		click("project.photo.name.txtbox");
		waitForPresent("project.photo.popup.ok.btn");
		clear("project.photo.popup.textbox.name");
		String name = "test" + common.getRandomValue();
		ConfigurationManager.getBundle().setProperty("photoName", name);
		sendKeys(name, "project.photo.popup.textbox");
		click("project.photo.popup.ok.btn");
		click("project.photo.save.btn");
	}

	@QAFTestStep(description = "User opens the saved image")
	public static void openSavedImage() {
		punchPage.tapOnImage();
		// click("rfi.promgmt.btn");
		// QAFExtendedWebElement ele = new
		// QAFExtendedWebElement("project.photo.mgmt.btn");
		// Point point = ele.getLocation();
		// int x = point.getX() + (ele.getSize().width / 2);
		// int y = point.getY() + (ele.getSize().height / 2);
		// TouchAction action = new TouchAction((MobileDriver) new
		// WebDriverTestBase().getDriver().getUnderLayingDriver());
		// action.tap(327, 130).perform();
		// System.out.println("=====================tapped on image
		// icon====================="+x+" "+y);
		// QAFTestBase.pause(50000);
		// if (new
		// QAFExtendedWebElement("project.mrkupanotation.viewall.btn").isPresent())
		// {
		// click("project.photo.tick.btn");
		//
		// } else {
		// click("project.mrkupanotation.hideall.btn");
		// click("rfi.promgmt.btn");
		// QAFExtendedWebElement element = new
		// QAFExtendedWebElement("project.photo.mgmt.btn");
		// Point pointhdall = element.getLocation();
		// int xhdall = pointhdall.getX() + (element.getSize().width / 2);
		// int yhdall = pointhdall.getY() + (element.getSize().height / 2);
		// TouchAction actionhdall = new TouchAction(
		// (MobileDriver) new
		// WebDriverTestBase().getDriver().getUnderLayingDriver());
		// actionhdall.tap(xhdall, yhdall).release().perform();
		// click("project.photo.tick.btn");
		//
		// }
		// punchPage.tapOnImage();
		punchPage.tapOnImage();
	}

	@QAFTestStep(description = "User should see the same saved image")
	public static void verifyPhoto() {
		Validator.verifyThat(new QAFExtendedWebElement("project.photo.header.txt").getAttribute("value")
				.equals(ConfigurationManager.getBundle().getProperty("photoName")), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "User rotate the image")
	public static void rotateImage() {
		QAFExtendedWebElement ele = new QAFExtendedWebElement("project.image");
		int wid = ele.getSize().width;
		int hei = ele.getSize().height;
		ConfigurationManager.getBundle().setProperty("oldWid", wid);
		ConfigurationManager.getBundle().setProperty("oldHei", hei);

		click("project.rotate.left.btn");
		click("project.rotate.save.btn");
	}

	@QAFTestStep(description = "User should see the roatated image")
	public static void verifyRotatedImage() {
		QAFExtendedWebElement ele = new QAFExtendedWebElement("project.image");
		int wid = ele.getSize().width;
		int hei = ele.getSize().height;

		Validator.verifyThat(
				(wid == Integer.parseInt(ConfigurationManager.getBundle().getProperty("oldWid").toString())),
				Matchers.equalTo(false));

		Validator.verifyThat(
				(hei == Integer.parseInt(ConfigurationManager.getBundle().getProperty("oldHei").toString())),
				Matchers.equalTo(false));
	}

	@QAFTestStep(description = "User should not see any anotaion")
	public static void verifyNoAoonotaionDisplayed() {
		click("project.mrkupanotation.btn");
		Validator.verifyThat(new QAFExtendedWebElement("project.mrkupanotation.txt").isDisplayed(),
				Matchers.equalTo(true));

	}

	@QAFTestStep(description = "User should see offline message")
	public static void verifyOfflineMessage() {
		Validator.verifyThat(
				new QAFExtendedWebElement("folder.offline.msg").getAttribute("label").contains("Unable to connect"),
				Matchers.equalTo(true));

	}

	@QAFTestStep(description = "User zoom the image")
	public static void zoom() {
		// TouchAction action = new TouchAction((MobileDriver) new
		// WebDriverTestBase().getDriver().getUnderLayingDriver());
		// action.press(100,300).release().perform().waitAction().press(100,300).release().perform();
		// }

		// new TouchAction((MobileDriver) new
		// WebDriverTestBase().getDriver().getUnderLayingDriver()).press(500,
		// 500).perform().release().press(0,0).perform();
		// Map<String, Object> params = new HashMap<>();
		// params.put("operation", "Zoom");
		// params.put("start", "50%,60%");
		// params.put("end", "40%,40%");
		// ((MobileDriver) new
		// WebDriverTestBase().getDriver().getUnderLayingDriver()).pi
		// .executeScript("mobile:touch:gesture", params);
		QAFExtendedWebElement element = new QAFExtendedWebElement("project.image");

		JavascriptExecutor js = (JavascriptExecutor) new WebDriverTestBase().getDriver().getUnderLayingDriver();
		Map<String, Object> params = new HashMap<>();
		params.put("scale", 10);
		params.put("velocity", 2);
		params.put("element", element.getId());
		js.executeScript("mobile: pinch", params);
		// new
		// WebDriverTestBase().getDriver().getUnderLayingDriver().execute_script("mobile:
		// pinch", {'element': element, 'scale' : 1.2, 'velocity' : 1.0});
		// JavascriptExecutor js = (JavascriptExecutor) new
		// WebDriverTestBase().getDriver().getUnderLayingDriver();
		// HashMap tapObject = new HashMap();
		// tapObject.put("x", String.valueOf(element.getSize().getWidth() / 2));
		// tapObject.put("y", String.valueOf(element.getSize().getHeight() /
		// 2));
		// tapObject.put("element", element.getId());
		// js.executeScript("mobile: pinch", "");
		// TouchAction action44 = new TouchAction(
		// (MobileDriver) new
		// WebDriverTestBase().getDriver().getUnderLayingDriver());

		// ((MobileDriver) new
		// WebDriverTestBase().getDriver().getUnderLayingDriver()).pinch(element.getSize().getWidth()/2,
		// element.getSize().getHeight()/2);

	}

	@QAFTestStep(description = "User hide the annotation")
	public static void hideAnnotation() {
		if (new QAFExtendedWebElement("project.annotation.collapse").isPresent()) {
			click("project.annotation.collapse.btn");
		}
	}

	@QAFTestStep(description = "User unhide the annotation")
	public static void unhideAnnotation() {

		if (new QAFExtendedWebElement("project.annotation.expand").isPresent()) {
			click("project.annotation.collapse.btn");
		}
	}

	@QAFTestStep(description = "User should not see the annotation tool bar")
	public static void hiddenAnotation() {

		Validator.verifyThat(new QAFExtendedWebElement("project.annotation.expand").isPresent(),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "User should see the annotation tool bar")
	public static void unhiddenAnnotation() {
		Validator.verifyThat(new QAFExtendedWebElement("project.annotation.collapse").isPresent(),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "User edit the annotation")
	public static void editAnnotation() {

		TouchAction action = new TouchAction((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
		action.tap(350, 635).perform();

		click("project.annotation.setting.toolbar");
		click("project.anno.color.btn");
		click("project.solidcolor.btn");

		// select color

		QAFExtendedWebElement ele = new QAFExtendedWebElement("project.color.selector");

		int x = (ele.getLocation().getX()) + 8;
		int y = (ele.getLocation().getY()) + 8;

		System.out.println("======================" + x);
		System.out.println("======================" + y);
		TouchAction action1 = new TouchAction(
				(MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());

		action1.tap(x, y).perform();

		click("project.anno.fill.color.btn");
		click("project.solidcolor.btn");

		// select color

		QAFExtendedWebElement ele1 = new QAFExtendedWebElement("project.color.selector");

		x = (ele1.getLocation().getX())+ 8;
		y = (ele1.getLocation().getY()) + 8;

		System.out.println("***************************" + x);
		System.out.println("***************************" + y);

		action.tap(x, y).perform();
		click("project.save.btn");

	}
}
