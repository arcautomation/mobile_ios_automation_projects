package com.skysite.project.ios.steps;

import static com.qmetry.qaf.automation.step.CommonStep.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.util.QAFWebDriverWait;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;

public class common {
	/**
	 * @param searchTerm
	 *            : search term to be searched
	 */
	@QAFTestStep(description = "User opens application")
	public static void openApp() {
		waitForNotPresent("project.loader.icon", 180);
		if (new QAFExtendedWebElement("edirpro.header.txt").isPresent()) {
			click("editpro.save.btn");
		}
		if (new QAFExtendedWebElement("banner.ok.button").isPresent()) {
			click("banner.ok.button");
		}

	}

	@QAFTestStep(description = "User should see the instruction screen")
	public static void instructionVerification() {
		verifyPresent("banner.skip.button");
		verifyPresent("banner.next.button");

	}

	@QAFTestStep(description = "User navigate back to the home screen")
	public static void navigateToHome() {
		if (new QAFExtendedWebElement("rfi.cancel.button").isPresent()) {
			click("rfi.cancel.button");
			Reporter.log("Clickced cancel button");
		}
		if (new QAFExtendedWebElement("rfi.close.button").isPresent()) {
			click("rfi.close.button");
			Reporter.log("Clickced Close button");
		}
		if (new QAFExtendedWebElement("project.back.btn").isPresent()) {
			click("project.back.btn");
			Reporter.log("Clickced back button");
		}

	}

	@QAFTestStep(description = "User skips the instructions")
	public static void skipInstruction() {
		waitForNotPresent("project.loader.icon", 180);

		try {
			waitForPresent("banner.skip.button", 8);
			if (new QAFExtendedWebElement("banner.next.button").isPresent()) {
				click("banner.skip.button");
			} else {
				click("banner.save.button");
			}

		} catch (Exception e) {
			Reporter.log("Instruction screen not available", MessageTypes.Info);
		}

	}

	public static void takeScrnShot(String filename, QAFExtendedWebElement ele) {
		File screenshot = ((TakesScreenshot) ((MobileDriver) new WebDriverTestBase().getDriver()
				.getUnderLayingDriver())).getScreenshotAs(OutputType.FILE);
		BufferedImage fullImg;
		try {
			fullImg = ImageIO.read(screenshot);
			// Get the location of element on the page
			Point point = ele.getLocation();

			// Get width and height of the element
			int eleWidth = ele.getSize().getWidth();
			int eleHeight = ele.getSize().getHeight();

			// Crop the entire page screenshot to get only element screenshot
			int dimenssion = Integer.parseInt(ConfigurationManager.getBundle().getProperty("dimenssion").toString());
			System.out.println(dimenssion);
			System.out.println(point.getX());
			System.out.println(point.getY());
			System.out.println(eleWidth);
			System.out.println(eleHeight);
			BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth + dimenssion,
					eleHeight + dimenssion);
			// BufferedImage eleScreenshot = fullImg.getSubimage(63, 64, 641,
			// 960);
			ImageIO.write(eleScreenshot, "png", screenshot);

			// Copy the element screenshot to disk

			File screenshotLocation = new File(filename);
			FileUtils.copyFile(screenshot, screenshotLocation);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void takeScrnShotZoom(String filename, QAFExtendedWebElement ele) {
		File screenshot = ((TakesScreenshot) ((MobileDriver) new WebDriverTestBase().getDriver()
				.getUnderLayingDriver())).getScreenshotAs(OutputType.FILE);
		BufferedImage fullImg;
		try {
			fullImg = ImageIO.read(screenshot);
			// Get the location of element on the page
			Point point = ele.getLocation();

			// Get width and height of the element
			int eleWidth = ele.getSize().getWidth();
			int eleHeight = ele.getSize().getHeight();

			// Crop the entire page screenshot to get only element screenshot
			int dimenssion = Integer.parseInt(ConfigurationManager.getBundle().getProperty("dimenssion").toString());
			System.out.println(dimenssion);
			System.out.println(point.getX());
			System.out.println(point.getY());
			System.out.println(eleWidth);
			System.out.println(eleHeight);
//			BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth + dimenssion,
//					eleHeight + dimenssion);
			 BufferedImage eleScreenshot = fullImg.getSubimage(63, 64, 1300,1800);
			ImageIO.write(eleScreenshot, "png", screenshot);

			// Copy the element screenshot to disk

			File screenshotLocation = new File(filename);
			FileUtils.copyFile(screenshot, screenshotLocation);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void swipeLeftToDelete(QAFWebElement ele) {
		int xEnd = ele.getLocation().getX();
		int y = ele.getLocation().getY() + 40;
		int xStart = xEnd + ele.getSize().width - 70;
		System.out.println("++++++++xStart" + xStart);
		System.out.println("++++++++xEnd" + xEnd);
		System.out.println("++++++++y" + y);

		// TouchAction action = new TouchAction((MobileDriver) new
		// WebDriverTestBase().getDriver().getUnderLayingDriver());
		System.out.println("+++++++++++++++++++++TouchActionDriver+++++++++++++++++");
		// action.longPress(xStart, y).moveTo(xEnd+20, y).release().perform();

//		((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver()).swipe(xStart, y, xEnd + 200, y,
//				4000);
		((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver()).swipe(372, 214, 200, 214,
				4000);
		;
	}

	public static long getRandomValue() {
		DateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
		Date date = new Date();
		return Long.parseLong(dateFormat.format(date));
	}

	public static void captureImage() {
		QAFTestBase.pause(3000);
		if (new QAFExtendedWebElement("banner.alloow.button").isPresent()) {
			click("banner.alloow.button");
		}
		if (new QAFExtendedWebElement("banner.camera.ok.button").isPresent()) {
			click("banner.camera.ok.button");
		}
		if (new QAFExtendedWebElement("banner.camera.ok.button").isPresent()) {
			click("banner.camera.ok.button");
		}

		waitForPresent("project.photo.capture.btn");
		click("project.photo.capture.btn");
		waitForPresent("project.photo.usephoto.btn");
		click("project.photo.usephoto.btn");
	}

	@QAFTestStep(description = "User click on {loc}")
	public static void userClick(String loc) {
		new QAFExtendedWebElement(loc).click();
	}

}
