package com.skysite.project.ios.steps;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;
import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;

public class gallery {
	static String filename;
	static List photoName = new ArrayList();
	static List<QAFWebElement> galSize;
	static String building =  "AutomationBuilding" ;
	static String level =  "AutomationLevel" ;
	static String room =  "AutomationRoom" ;
	static String area =  "AutomationArea" ;
	static String description =  "AutomationDescription" ;
	

	@QAFTestStep(description = "User create a new folder in gallery")
	public static void createAlbum() {
		filename = common.getRandomValue() + "";
		click("gallery.addfolder.btn");
		sendKeys(filename, "gallery.albumname.txtbox");
		click("gallery.create.btn");

	}

	@QAFTestStep(description = "User select created album")
	public static void verifyAlbum() {
		QAFExtendedWebElement ele = new QAFExtendedWebElement(
				String.format(getBundle().getString("project.folder.name.txt"), filename, filename));

		if (!ele.isPresent()) {
			TouchAction action = new TouchAction(
					(MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
			action.press(276, 577).moveTo(300, 300).release().perform();
		}

		ele.click();
	}

	@QAFTestStep(description = "User add photos")
	public static void addPhotoAlbum() {
		galSize = new WebDriverTestBase().getDriver().findElements("gallary.photo.name.list");
		click("gallery.addphoto.btn");
		common.captureImage();
		QAFTestBase.pause(2000);
		List<QAFWebElement> photoList = new WebDriverTestBase().getDriver().findElements("gallary.photo.name.list");

		for (int i = 0; i < photoList.size(); i++) {
			photoName.add(photoList.get(i).getAttribute("value"));
		}

		System.out.println(photoName);
		// ConfigurationManager.getBundle().setProperty("photoName",
		// photoList.get(0).getAttribute("value"));

	}

	@QAFTestStep(description = "User should see the added photo in the album")
	public static void verifyPhotoAdded() {

		Validator.verifyThat(photoName.contains(new WebDriverTestBase().getDriver()
				.findElements("gallary.photo.name.list").get(0).getAttribute("value")), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "User should see the added photo in the gallery")
	public static void verifyPhotoAddedInGallery() {

		List<QAFWebElement> galleryList = new WebDriverTestBase().getDriver().findElements("gallary.photo.name.list");
		System.out.println(galleryList.size());
		System.out.println(galSize.size());
		Validator.verifyThat(galleryList.size() == (galSize.size() + 1), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "User selects mutiple image to upload")
	public static void selectMultipleImage() {
		click("gallery.addphoto.btn");
		click("gallary.camera.gallary.btn");
		click("gallary.allphoto.btn");
		List<QAFWebElement> photoList = new WebDriverTestBase().getDriver().findElements("gallary.allphoto.imglist");
		photoList.get(0).click();
		photoList.get(1).click();
		click("gallary.allphoto.done.btn");

	}

	@QAFTestStep(description = "User should see all the uploaded images")
	public static void verifyMultipleImage() {
		List<QAFWebElement> galleryList = new WebDriverTestBase().getDriver().findElements("gallary.photo.name.list");

		Validator.verifyThat(galleryList.size() == 2, Matchers.equalTo(true));
	}

	@QAFTestStep(description = "User select album to rename")
	public static void selectImageOfFolder() {
		List<QAFWebElement> photoList = new WebDriverTestBase().getDriver().findElements("gallary.photo.name.list");

		TouchAction action = new TouchAction((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
		action.longPress(photoList.get(0)).release();
		action.perform();
		QAFTestBase.pause(5000);

	}
	@QAFTestStep (description = "User rename the album")
	public static void renameLbum(){
		click("gallary.rename.btn");
		click("gallery.name.clear.btn");
		String fName = common.getRandomValue() + "";
		sendKeys(fName, "gallery.albumname.txtbox");
		ConfigurationManager.getBundle().setProperty("fileName", fName);
		click("gallery.update.btn");
	}
	
	@QAFTestStep (description = "User should see the update album")
	public static void verifyUpdatedAlbum(){
		List<QAFWebElement> albumList = new WebDriverTestBase().getDriver().findElements("gallary.photo.name.list");
		String name = ConfigurationManager.getBundle().getProperty("fileName").toString();
		List<String> albumName = new ArrayList<String>();
		for (int i = 0; i < albumList.size(); i++) {
			albumName.add(albumList.get(i).getAttribute("value"));
		}
		Validator.verifyThat(albumName.contains(name), Matchers.equalTo(true));
	}
	@QAFTestStep (description = "User enters the attributes of the image")
	public static void enterAttributes(){
		sendKeys(building, "gallery.cd.building.txtbox");
		sendKeys(level, "gallery.cd.level.txtbox");
		sendKeys(room, "gallery.cd.room.txtbox");
		sendKeys(area, "gallery.cd.area.txtbox");
		sendKeys(description, "gallery.cd.desc.txtbox");
	}
	
	@QAFTestStep (description = "User copydown the image attributes")
	public static void performCopydown(){
		click("gallary.mark.btn");
		List<QAFWebElement> imageList = new WebDriverTestBase().getDriver().findElements("gallary.photo.name.list");
		imageList.get(0).click();
		imageList.get(1).click();
		click("gallary.editattri.btn");
		
		enterAttributes();
		
		click("gallery.cd.building.btn");
		click("gallery.cd.level.btn");
		click("gallery.cd.room.btn");
		click("gallery.cd.area.btn");
		click("gallery.cd.desc.btn");
		
		click("gallery.cd.done.btn");
		
		
		
	}
	@QAFTestStep (description = "User should see the same attributes in all the images")
	public static void verifyCopydown(){
		
		
		List<QAFWebElement> buildingTxtBoxList = new WebDriverTestBase().getDriver().findElements("gallery.cd.building.txtbox");
		List<QAFWebElement> levelTxtBoxList = new WebDriverTestBase().getDriver().findElements("gallery.cd.level.txtbox");
		List<QAFWebElement> roomTxtBoxList = new WebDriverTestBase().getDriver().findElements("gallery.cd.room.txtbox");
		List<QAFWebElement> areaTxtBoxList = new WebDriverTestBase().getDriver().findElements("gallery.cd.area.txtbox");
		List<QAFWebElement> descTxtBoxList = new WebDriverTestBase().getDriver().findElements("gallery.cd.desc.txtbox");
		
		for (int i =0; i<buildingTxtBoxList.size();i++){
			Validator.verifyThat(buildingTxtBoxList.get(i).getAttribute("value").contains(building), Matchers.equalTo(true));
			Validator.verifyThat(levelTxtBoxList.get(i).getAttribute("value").contains(level), Matchers.equalTo(true));
			Validator.verifyThat(roomTxtBoxList.get(i).getAttribute("value").contains(room), Matchers.equalTo(true));
			Validator.verifyThat(areaTxtBoxList.get(i).getAttribute("value").contains(area), Matchers.equalTo(true));
			Validator.verifyThat(descTxtBoxList.get(i).getAttribute("value").contains(description), Matchers.equalTo(true));
			
		}
	
	}
	
	@QAFTestStep(description = "User tap on the image and add photo from local storage")
	public static void addPhotoFromLocal(){
		punchPage.tapOnImage();
		click("gallary.camera.gallary.btn");
		click("gallary.allphoto.btn");
		List<QAFWebElement> photoList = new WebDriverTestBase().getDriver().findElements("gallary.allphoto.imglist");
		photoList.get(0).click();
		photoList.get(1).click();
		click("gallary.allphoto.done.btn");
	}
}
