package com.skysite.project.ios.steps;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;
import static com.qmetry.qaf.automation.step.CommonStep.*;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;

public class punchPage {

	@QAFTestStep(description = "User click on punch")
	public static void clickPunch() {
		click("punch.drawer.btn");
	}

	public static void tapOnImage() {
		TouchAction action = new TouchAction((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
		action.press(276, 577).release().perform();
	}

	@QAFTestStep(description = "User tap on the image and create punch")
	public static void tapImgtoPunch() {
		tapOnImage();
		if (new QAFExtendedWebElement("punch.allow.btn").isPresent()) {
			click("punch.allow.btn");
		}
		createPunch();

	}

	public static void main(String[] args) {
		DateFormat dateFormat = new SimpleDateFormat("MM");
		Date date = new Date();
		int d = Integer.parseInt(dateFormat.format(date));
		System.out.println(d);
	}

	@QAFTestStep(description = "User opens the saved punch")
	public static void openSavedPunch() {
		click("project.mrkupanotation.btn");
		click("punch.promgmt.btn");

		if (new QAFExtendedWebElement("project.mrkupanotation.viewall.btn").isPresent()) {
			click("project.mrkupanotation.viewall.btn");
			// click("punch.promgmt.btn");
			click("project.mrkupanotation.btn");
			click("project.mrkupanotation.hideall.btn");
		} else {
			click("project.mrkupanotation.hideall.btn");
		}
		click("project.mrkupanotation.btn");
		// click("punch.promgmt.btn");
		List<QAFWebElement> openBtnList = new WebDriverTestBase().getDriver().findElements("punch.open.btn");
		openBtnList.get(0).click();

	}

	@QAFTestStep(description = "User should see the same saved punch")
	public static void verifyPunch() {
		String stamp = new QAFExtendedWebElement("punch.popup.stamp.txt").getAttribute("value");
		String to = new QAFExtendedWebElement("punch.popup.to.txt").getAttribute("value");
		String description = new QAFExtendedWebElement("punch.popup.desc.txt").getAttribute("value");

		System.out.println("Stamp:" + stamp);
		System.out.println("Save Stamp:" + ConfigurationManager.getBundle().getProperty("punchStamp"));

		System.out.println("to:" + to);
		System.out.println("Save to:" + ConfigurationManager.getBundle().getProperty("punchTo"));

		System.out.println("description:" + description);
		System.out.println("Save description:" + ConfigurationManager.getBundle().getProperty("newDesc"));

		Validator.verifyThat(stamp.equals(ConfigurationManager.getBundle().getProperty("punchStamp")),
				Matchers.equalTo(true));

		Validator.verifyThat(to.equals(ConfigurationManager.getBundle().getProperty("punchTo")),
				Matchers.equalTo(true));

		Validator.verifyThat(description.equals(ConfigurationManager.getBundle().getProperty("newDesc")),
				Matchers.equalTo(true));

	}

	public static void createPunch() {
		click("punch.choosestamp.dropdwn");
		if (!new QAFExtendedWebElement("punch.stamp.list").isPresent()) {
			sendKeys("Tst", "punch.choosestamp.shortname");
			sendKeys("Automation", "punch.choosestamp.name");
			click("punch.stamp.create.btn");
		} else {
			List<QAFWebElement> toList = new WebDriverTestBase().getDriver().findElements("punch.stamp.list");
			toList.get(0).click();
		}

		// select to drop down
		QAFTestBase.pause(2000);
		click("punch.to.dropdwn");
		sendKeys(ConfigurationManager.getBundle().getProperty("punchTo").toString(), "punch.to.search.txtbox");
		click("punch.to.item.list");

		click("punch.calender.icon");

		// select date
		DateFormat dateFormat = new SimpleDateFormat("d");
		Date date = new Date();
		int d = Integer.parseInt(dateFormat.format(date));

		DateFormat monthFormat = new SimpleDateFormat("MM");
		Date month = new Date();
		int m = Integer.parseInt(dateFormat.format(month));

		if (d >= 22) {
			d = 1;
			click("punch.calender.right");
		}
		String calDate = Integer.toString(d + 7);
		new QAFExtendedWebElement(String.format(getBundle().getString("punch.calender.date.btn"), calDate, calDate))
				.click();

		// Enter description
		click("punch.description.txtbox");

		String desc = common.getRandomValue() + ConfigurationManager.getBundle().getProperty("PunchDesc").toString();
		ConfigurationManager.getBundle().setProperty("newDesc", desc);
		sendKeys(desc, "punch.description.txtbox");

		// CLick create
		// click("punch.create.button");
		// QAFTestBase.pause(2000);
		// click("punch.popup.createconfirm.btn");
	}

	@QAFTestStep(description = "User create punch on project level")
	public static void createRFIProLevel() {
		click("punch.projectlvl.allpunch.btn");
		click("punch.projectlvl.create.btn");
		createPunch();

	}

	@QAFTestStep(description = "User should see the created punch on project level")
	public static void verifyProjectLvlPunch() {
		click("punch.projectlvl.allpunch.btn");
		click("punch.projectlvl.open.btn.list");
		verifyPunch();

	}

	@QAFTestStep(description = "User attach image in punch")
	public static void attachImageToPunch() {
		click("punch.attachment.cmr.btn");
		common.captureImage();
		ConfigurationManager.getBundle().setProperty("attachmentName",
				new QAFExtendedWebElement("punch.attach.filename.txt").getAttribute("value"));
	}

	@QAFTestStep(description = "User should see tha attached image")
	public static void verifyAAttachment() {
		Validator.verifyThat(
				new QAFExtendedWebElement("punch.attaced.filename.txt").getAttribute("value")
						.contains(ConfigurationManager.getBundle().getProperty("attachmentName").toString()),
				Matchers.equalTo(true));

	}

}
