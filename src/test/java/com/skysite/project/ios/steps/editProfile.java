package com.skysite.project.ios.steps;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.*;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;

import java.util.List;
import java.util.Random;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.testng.report.Report;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class editProfile {

	static String fullName = "Fname" + common.getRandomValue();
	static String phone = "" + common.getRandomValue();
	static String city = "America" + common.getRandomValue();
	static String jobTitle = "jobtitle" + common.getRandomValue();
	static String fax = "" + common.getRandomValue();
	static String zip = "" + common.getRandomValue();

	// @QAFTestStep(description = "User fill the edit profile pop up and save
	// it")
	// public static void editProfile(){
	//
	// }

	@QAFTestStep(description = "User edit and save the profile detail")
	public static void editProfile() {
		List<QAFWebElement> textBoxList = new WebDriverTestBase().getDriver().findElements("editrpro.fullname.txt");
		List<QAFWebElement> textBox2List = new WebDriverTestBase().getDriver().findElements("editrpro.jobtitle.txt");

		// Edit Fullname
		textBoxList.get(0).clear();
		textBoxList.get(0).sendKeys(fullName);

		// Edit job totle
		textBox2List.get(0).clear();
		textBox2List.get(0).sendKeys(jobTitle);

		// Edit Company type dropdown
		textBoxList.get(1).click();
		QAFTestBase.pause(2000);
		List<QAFWebElement> dropDownList = new WebDriverTestBase().getDriver()
				.findElements("editrpro.companytype.dropdwn.list");
		System.out.println("+++++++++++++++++++++++++++" + dropDownList.size());
		Random rn = new Random();
		int numb = rn.nextInt(2) + 1;
		Reporter.log("RandomNumber is: " + numb);
		dropDownList.get(numb).click();

		ConfigurationManager.getBundle().setProperty("updatedCompanyType",
				textBoxList.get(1).getAttribute("value"));

		// Edit Phone
		textBoxList.get(2).clear();
		textBoxList.get(2).sendKeys(phone);

		// Edit Fax
		textBox2List.get(2).clear();
		textBox2List.get(2).sendKeys(fax);
		;

		// Edit City
		textBoxList.get(3).clear();
		textBoxList.get(3).sendKeys(city);

		// Edit zip code
		textBox2List.get(3).clear();
		textBox2List.get(3).sendKeys(zip);

		// Edit Country dropdown
		textBoxList.get(4).click();
		QAFTestBase.pause(2000);
		dropDownList.get(numb).click();
		ConfigurationManager.getBundle().setProperty("updatedCountry", textBoxList.get(4).getAttribute("value"));

		// Edit state
		textBox2List.get(4).click();
		QAFTestBase.pause(2000);
		dropDownList.get(0).click();
		ConfigurationManager.getBundle().setProperty("updatedSTate", textBox2List.get(4).getAttribute("value"));

	}

	@QAFTestStep(description = "User should see the updated profile")
	public static void verifyUpdatedProfile() {

		List<QAFWebElement> textBoxList = new WebDriverTestBase().getDriver().findElements("editrpro.fullname.txt");
		List<QAFWebElement> textBox2List = new WebDriverTestBase().getDriver().findElements("editrpro.jobtitle.txt");

		Validator.verifyThat(textBoxList.get(0).getAttribute("value").equals(fullName), Matchers.equalTo(true));
		Validator.verifyThat(textBox2List.get(0).getAttribute("value").equals(jobTitle), Matchers.equalTo(true));
		System.out.println("==========================="+ConfigurationManager.getBundle().getProperty("updatedCompanyType"));
		Validator.verifyThat(textBoxList.get(1).getAttribute("value")
				.equals(ConfigurationManager.getBundle().getProperty("updatedCompanyType")), Matchers.equalTo(true));
		Validator.verifyThat(textBoxList.get(2).getAttribute("value").equals(phone), Matchers.equalTo(true));
		Validator.verifyThat(textBox2List.get(2).getAttribute("value").equals(fax), Matchers.equalTo(true));
		Validator.verifyThat(textBoxList.get(3).getAttribute("value").equals(city), Matchers.equalTo(true));
		Validator.verifyThat(textBox2List.get(3).getAttribute("value").equals(zip), Matchers.equalTo(true));
		Validator.verifyThat(textBoxList.get(4).getAttribute("value")
				.equals(ConfigurationManager.getBundle().getProperty("updatedCountry")), Matchers.equalTo(true));
		Validator.verifyThat(textBox2List.get(4).getAttribute("value")
				.equals(ConfigurationManager.getBundle().getProperty("updatedSTate")), Matchers.equalTo(true));
		System.out.println("==========================="+ConfigurationManager.getBundle().getProperty("updatedCountry"));
		System.out.println("==========================="+ConfigurationManager.getBundle().getProperty("updatedSTate"));
		
	}

}
