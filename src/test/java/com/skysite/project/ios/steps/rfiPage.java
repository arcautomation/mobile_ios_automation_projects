package com.skysite.project.ios.steps;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;
import static com.qmetry.qaf.automation.step.CommonStep.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.hamcrest.Matchers;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;

public class rfiPage {

	@QAFTestStep(description = "User click on rfi")
	public static void clickRfi() {
		click("rfi.drawer.btn");
	}
	@QAFTestStep(description = "User tap on the image")
			public static void taponImageForRfi(){
		punchPage.tapOnImage();
	}
	

	@QAFTestStep(description = "User create rfi")
	public static void tapImgtoRfi() {
		

		// enter data in to field
		click("rfi.to.dropdwn");
		sendKeys(ConfigurationManager.getBundle().getProperty("rfiTo").toString(), "rfi.to.search.txtbox");
		click("rfi.to.list");

		// enter cc
		click("rfi.cc.dropdwn");
		sendKeys(ConfigurationManager.getBundle().getProperty("rfiCc").toString(), "rfi.to.search.txtbox");
		click("rfi.cc.list");

		// enter subject
		punchPage.tapOnImage();
		String subject = "Subject" + common.getRandomValue();
		ConfigurationManager.getBundle().setProperty("newSubject", subject);
		sendKeys(subject, "rfi.subject.txtbox");

		// enter discipline
		// String disc = "disc "+ common.getRandomValue();
		// ConfigurationManager.getBundle().setProperty("newDisc", disc);
		// sendKeys(disc, "rfi.discipline.txtbox");

		// enter specification
		String specifiction = "spec " + common.getRandomValue();
		ConfigurationManager.getBundle().setProperty("newSpec", specifiction);
		sendKeys(specifiction, "rfi.specification.txtbox");

		// check Cost Impact check box
		click("rfi.costimpact.chkbox");

		// check schedule cost impact
		click("rfi.scheduleimpact.chkbox");

		// enter question
		String question = "Automation Question" + common.getRandomValue();
		ConfigurationManager.getBundle().setProperty("newQuestion", question);
		sendKeys(question, "rfi.question.txtbox");

		// scroll down to find the attachment
		// QAFExtendedWebElement ele = new
		// QAFExtendedWebElement("rfi.popup.window");
		// Point point = ele.getLocation();
		// int startx = point.getX() + (ele.getSize().width / 2);
		// int starty = point.getY() + (ele.getSize().height / 2);
		// int endy = starty + 50;
		//
		// TouchAction action = new TouchAction((MobileDriver) new
		// WebDriverTestBase().getDriver().getUnderLayingDriver());
		// action.press(startx, starty).moveTo(startx,
		// endy).release().perform();

		// add external attachment
		QAFTestBase.pause(2000);
		click("rfi.attachphot.btn");
		click("gallary.allphoto.btn");
		List<QAFWebElement> photoList = new WebDriverTestBase().getDriver().findElements("gallary.allphoto.imglist");
		photoList.get(0).click();
		click("gallary.allphoto.done.btn");
//		common.captureImage();
		String imageName = new QAFExtendedWebElement("rfi.filename.txtbox").getAttribute("value");
		ConfigurationManager.getBundle().setProperty("newImageName", imageName);

		click("rfi.create.button");
		QAFTestBase.pause(2000);
	}
	
	@QAFTestStep(description = "User create rfi on offline mode")
	public static void tapImgtoRfiinOfflineMode() {
		

//		// enter data in to field
//		click("rfi.to.dropdwn");
//		sendKeys(ConfigurationManager.getBundle().getProperty("rfiTo").toString(), "rfi.to.search.txtbox");
//		click("rfi.to.list");
//
//		// enter cc
//		click("rfi.cc.dropdwn");
//		sendKeys(ConfigurationManager.getBundle().getProperty("rfiCc").toString(), "rfi.to.search.txtbox");
//		click("rfi.cc.list");

		// enter subject
		punchPage.tapOnImage();
		String subject = "Subject" + common.getRandomValue();
		ConfigurationManager.getBundle().setProperty("newSubject", subject);
		sendKeys(subject, "rfi.subject.txtbox");

		// enter specification
		String specifiction = "spec " + common.getRandomValue();
		ConfigurationManager.getBundle().setProperty("newSpec", specifiction);
		sendKeys(specifiction, "rfi.specification.txtbox");

		// check Cost Impact check box
		click("rfi.costimpact.chkbox");

		// check schedule cost impact
		click("rfi.scheduleimpact.chkbox");

		// enter question
		String question = "Automation Question" + common.getRandomValue();
		ConfigurationManager.getBundle().setProperty("newQuestion", question);
		sendKeys(question, "rfi.question.txtbox");

		// scroll down to find the attachment
		// QAFExtendedWebElement ele = new
		// QAFExtendedWebElement("rfi.popup.window");
		// Point point = ele.getLocation();
		// int startx = point.getX() + (ele.getSize().width / 2);
		// int starty = point.getY() + (ele.getSize().height / 2);
		// int endy = starty + 50;
		//
		// TouchAction action = new TouchAction((MobileDriver) new
		// WebDriverTestBase().getDriver().getUnderLayingDriver());
		// action.press(startx, starty).moveTo(startx,
		// endy).release().perform();

		// add external attachment
		QAFTestBase.pause(2000);
		click("rfi.attachphot.btn");
		click("gallary.allphoto.btn");
		List<QAFWebElement> photoList = new WebDriverTestBase().getDriver().findElements("gallary.allphoto.imglist");
		photoList.get(0).click();
		click("gallary.allphoto.done.btn");
//		common.captureImage();
		String imageName = new QAFExtendedWebElement("rfi.filename.txtbox").getAttribute("value");
		ConfigurationManager.getBundle().setProperty("newImageName", imageName);

		click("rfi.create.button");
		QAFTestBase.pause(2000);
	}

	public static void openRfiProMgmt() {
		TouchAction action = new TouchAction((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
		action.tap(195, 127).perform();
	}

	@QAFTestStep(description = "User opens the saved RFI")
	public static void openSavedRfi() {
		
		//Code to select RFI from the RFI menu list
		
//		click("project.back.btn");
//		click("project.menu.btn");
//		click("rfi.menu.rfi.btn");
//		
//		List<QAFWebElement> rfiList = new WebDriverTestBase().getDriver().findElements("rfi.list");
//		rfiList.get(0).click();
		
		
		//Code to select RFI from the annotation top navigation panel
		
		click("project.mrkupanotation.btn");
		click("rfi.promgmt.btn");

		if (new QAFExtendedWebElement("project.mrkupanotation.viewall.btn").isPresent()) {
			click("project.mrkupanotation.viewall.btn");
			// click("punch.promgmt.btn");
			click("project.mrkupanotation.btn");
			click("project.mrkupanotation.hideall.btn");
		} else {
			click("project.mrkupanotation.hideall.btn");
		}
		click("project.mrkupanotation.btn");
		// click("punch.promgmt.btn");
		click("punch.open.btn");

		// click("project.mrkupanotation.btn");
		// click("rfi.promgmt.btn");
		// openRfiProMgmt();
		// if (new
		// QAFExtendedWebElement("project.mrkupanotation.viewall.btn").isPresent())
		// {
		// click("project.mrkupanotation.viewall.btn");
		// click("project.mrkupanotation.btn");
		// openRfiProMgmt();
		// click("project.mrkupanotation.hideall.btn");
		// } else {
		// click("project.mrkupanotation.hideall.btn");
		// }
		// click("rfi.promgmt.btn");
		// openRfiProMgmt();
		// click("rfi.open.btn");
		// Dimension dim = new
		// WebDriverTestBase().getDriver().getUnderLayingDriver().manage().window().getSize();
		// int x = (int) (0.22 * dim.getHeight());
		// int y = (int) (0.10 * dim.getHeight());

	}

	@QAFTestStep(description = "User opens the drafted RFI")
	public static void openDraftedRfi() {
//		click("project.back.btn");
//		click("project.menu.btn");
//		click("rfi.menu.rfi.btn");
//		
//		List<QAFWebElement> rfiList = new WebDriverTestBase().getDriver().findElements("rfi.list");
//		rfiList.get(0).click();
		click("project.mrkupanotation.btn");
		click("rfi.promgmt.btn");

		if (new QAFExtendedWebElement("project.mrkupanotation.viewall.btn").isPresent()) {
			click("project.mrkupanotation.viewall.btn");
			// click("punch.promgmt.btn");
			click("project.mrkupanotation.btn");
			click("project.mrkupanotation.hideall.btn");
		} else {
			click("project.mrkupanotation.hideall.btn");
		}
		click("project.mrkupanotation.btn");
		click("rfi.draft.btn");
		 Dimension dim = new
		 WebDriverTestBase().getDriver().getUnderLayingDriver().manage().window().getSize();
		 int x = (int) (0.22 * dim.getHeight());
		 int y = (int) (0.10 * dim.getHeight());

	}

	@QAFTestStep(description = "User should see the same saved RFI")
	public static void verifyRFI() {

		// Status verification
		String status = new QAFExtendedWebElement("rfi.status.txt").getAttribute("value");
		Validator.verifyThat(status.equals("Open"), Matchers.equalTo(true));
		Reporter.log("Status matched");

		// Subject Verification
		String subject = new QAFExtendedWebElement("rfi.popup.subject").getAttribute("value").split("\\s+")[1];
		Validator.verifyThat(subject.equals(ConfigurationManager.getBundle().getProperty("newSubject")),
				Matchers.equalTo(true));
		Reporter.log("Subject matched");

		// To verification
		String to = new QAFExtendedWebElement("rfi.popup.to").getAttribute("value").split(":")[1].trim();
		Validator.verifyThat(to.equals(ConfigurationManager.getBundle().getProperty("rfiTo")), Matchers.equalTo(true));
		Reporter.log("To matched");

		// Question verification
		String question = new QAFExtendedWebElement("rfi.popup.question").getAttribute("value").split(":")[1].trim();
		Validator.verifyThat(question.equals(ConfigurationManager.getBundle().getProperty("newQuestion")),
				Matchers.equalTo(true));
		Reporter.log("Question matched");

		// CC verification
		String cc = new QAFExtendedWebElement("rfi.popup.cc").getAttribute("value").split(":")[1].trim();
		Validator.verifyThat(cc.equals(ConfigurationManager.getBundle().getProperty("rfiCc")), Matchers.equalTo(true));
		Reporter.log("CC matched");

		// Specification verification
		String spec = new QAFExtendedWebElement("rfi.popup.spec").getAttribute("value").split(":")[1].trim();
		Validator.verifyThat(spec.equals(ConfigurationManager.getBundle().getProperty("newSpec")),
				Matchers.equalTo(true));
		Reporter.log("Specification matched");

		// Potential cost verification
		String potCost = new QAFExtendedWebElement("rfi.popup.potcostimp").getAttribute("value").split(":")[1].trim();
		Validator.verifyThat(potCost.equalsIgnoreCase("YES"), Matchers.equalTo(true));
		Reporter.log("Potential cost impact matched");

		// Potential schedule verification
		String potSch = new QAFExtendedWebElement("rfi.popup.potschimp").getAttribute("value").split(":")[1].trim();
		Validator.verifyThat(potSch.equalsIgnoreCase("YES"), Matchers.equalTo(true));
		Reporter.log("Potential cost impact matched");

		QAFExtendedWebElement ele = new QAFExtendedWebElement("rfi.popup.window");
		Point point = ele.getLocation();
		int startx = point.getX() + (ele.getSize().width / 2);
		int starty = point.getY() + (ele.getSize().height / 2);
		int endy = starty + 350;
		TouchAction action = new TouchAction((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
		action.press(startx, starty).moveTo(startx, endy).release().perform();

		// Attachment verification
		String attachment = new QAFExtendedWebElement("rfi.popup.attachment").getAttribute("value").trim();
		Validator.verifyThat(attachment.equals(ConfigurationManager.getBundle().getProperty("newImageName")),
				Matchers.equalTo(true));
		Reporter.log("Attachment matched");
	}

	@QAFTestStep(description = "User should see the same drafted RFI")
	public static void verifyDraftedRFI() {
		// Subject Verification
		String subject = new QAFExtendedWebElement("rfi.draft.subject").getAttribute("value");
		Validator.verifyThat(subject.equals(ConfigurationManager.getBundle().getProperty("newSubject")),
				Matchers.equalTo(true));
		Reporter.log("Subject matched");

//		// To verification
//		String to = new QAFExtendedWebElement("rfi.draft.to").getAttribute("value");
//		Validator.verifyThat(to.equals(ConfigurationManager.getBundle().getProperty("rfiToName")),
//				Matchers.equalTo(true));
//		Reporter.log("To matched");
//
//		// CC verification
//		String cc = new QAFExtendedWebElement("rfi.draft.cc").getAttribute("value");
//		Validator.verifyThat(cc.equals(ConfigurationManager.getBundle().getProperty("rfiCcName")),
//				Matchers.equalTo(true));
//		Reporter.log("Cc matched");

		// Specification verification
		String spec = new QAFExtendedWebElement("rfi.draft.spec").getAttribute("value");
		Validator.verifyThat(spec.equals(ConfigurationManager.getBundle().getProperty("newSpec")),
				Matchers.equalTo(true));
		Reporter.log("Specification matched");

//		// Potential cost impact verification
//		String pCostValue = new QAFExtendedWebElement("rfi.costimpact.chkbox").getAttribute("value");
//		Validator.verifyThat(pCostValue.equals("1"), Matchers.equalTo(true));
//		Reporter.log("Potential cost impact verification matched");

		// Potential scheduled impact verification
		String pScheduleValue = new QAFExtendedWebElement("rfi.scheduleimpact.chkbox").getAttribute("value");
		Validator.verifyThat(pScheduleValue.equals("1"), Matchers.equalTo(true));
		Reporter.log("Potential scheduled impact verification matched");

		// Image verification
		click("rfi.draft.question");
		String attachment = new QAFExtendedWebElement("rfi.draft.image").getAttribute("value").trim();
		Validator.verifyThat(attachment.equals(ConfigurationManager.getBundle().getProperty("newImageName")),
				Matchers.equalTo(true));
		Reporter.log("Image matched");

		// Question verification
		String question = new QAFExtendedWebElement("rfi.draft.question").getAttribute("value");
		Validator.verifyThat(question.equals(ConfigurationManager.getBundle().getProperty("newQuestion")),
				Matchers.equalTo(true));
		Reporter.log("Question matched");

	}

	@QAFTestStep(description = "User hides all RFI")
	public static void hideAllRfi() {
		click("project.mrkupanotation.btn");
		click("rfi.promgmt.btn");

		if (new QAFExtendedWebElement("project.mrkupanotation.viewall.btn").isPresent()) {
			click("project.mrkupanotation.viewall.btn");
			// click("punch.promgmt.btn");
			click("project.mrkupanotation.btn");
			click("project.mrkupanotation.hideall.btn");
		} else {
			click("project.mrkupanotation.hideall.btn");
		}
	}

	@QAFTestStep(description = "User answer the RFI question and add image")
	public static void answerQuestionRFI() {
		sendKeys("Answer" + common.getRandomValue(), "rfi.answer.txtbox");
		
		click("rfi.answer.image.button");
		common.captureImage();
//		String imageName = new QAFExtendedWebElement("rfi.answer.image.txt").getAttribute("value");
//		ConfigurationManager.getBundle().setProperty("answerImageName", imageName);
		
		click("rfi.answer.reply.btn");
	}

	@QAFTestStep(description = "User opens the responded rfi")
	public static void openRespondedRFI() {
		click("project.mrkupanotation.btn");
		click("rfi.promgmt.btn");
		waitForPresent("rfi.responded.btn");
		click("rfi.responded.btn");
	}

	@QAFTestStep(description = "User should see tha same responded rfi")
	public static void verifyRespondedRFI() {
		// Status verification
		String status = new QAFExtendedWebElement("rfi.savedanswer.txt").getAttribute("value");
		Reporter.log("before matched");
		Validator.verifyThat(status.contains("Responded"), Matchers.equalTo(true));
		Reporter.log("Status matched");
	}
	
	@QAFTestStep(description = "User open the saved rfi from prject screen")
	public static void openRfiFromHome(){
//		click("project.menu.btn");
//		click("project.menu.btn");
//		click("rfi.menu.rfi.btn");
		click("rfi.myrfi.btn");
		List<QAFWebElement> rfiList = new WebDriverTestBase().getDriver().findElements("rfi.list");
		rfiList.get(0).click();
	}

}
