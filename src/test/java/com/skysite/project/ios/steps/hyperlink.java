package com.skysite.project.ios.steps;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;
import static com.qmetry.qaf.automation.step.CommonStep.*;

import org.hamcrest.Matchers;
import org.openqa.selenium.Point;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;

public class hyperlink {

	@QAFTestStep(description = "User opne the hyperlink drawer")
	public static void openHyperlink() {
		waitForPresent("hl.drawer.btn");
		QAFTestBase.pause(3000);
		click("hl.drawer.btn");
		waitForPresent("hl.circle.drawer.btn");
		click("hl.circle.drawer.btn");

	}

	@QAFTestStep(description = "User create a hyperlink to {foldername} folder")
	public static void createHyperlinkToFolder(String fileName) {
		System.out.println(fileName);
		TouchAction action = new TouchAction((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
		action.press(276, 577).moveTo(100, 100).release().perform();
		String folderName = ConfigurationManager.getBundle().getProperty(fileName).toString();
		sendKeys(folderName, "hl.popup.search.txtbox");
		click("hl.keypad.search.btn");
		new QAFExtendedWebElement(String.format(getBundle().getString("hl.folder.list"), folderName, folderName))
				.click();

		String fName = new QAFExtendedWebElement("hl.file.name").getAttribute("name");
		ConfigurationManager.getBundle().setProperty("fname", fName);
		click("hl.link.btn");
		tapHyperlink();
	}

	@QAFTestStep(description = "User create a hyperlink to file of {folderName}")
	public static void createHyperlink(String fileName) {
		System.out.println(fileName);
		TouchAction action = new TouchAction((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
		action.press(276, 577).moveTo(100, 100).release().perform();

		System.out.println(fileName.split("&").length);

		for (int i = 0; i < fileName.split("&").length; i++) {
			System.out.println("inside for");
			System.out.println(fileName.split("&")[i]);
			String folderName = ConfigurationManager.getBundle().getProperty(fileName.split("&")[i].trim()).toString();
			System.out.println("++++++++++++" + folderName);
			// clear("hl.popup.search.txtbox");
			if (i > 0) {
				QAFTestBase.pause(2000);
				clear("hl.search.close");
			}
			sendKeys(folderName, "hl.popup.search.txtbox");
			click("hl.keypad.search.btn");

			new QAFExtendedWebElement(String.format(getBundle().getString("hl.folder.list"), folderName, folderName))
					.click();

		}

		String fName = new QAFExtendedWebElement("hl.file.name").getAttribute("value");
		ConfigurationManager.getBundle().setProperty("fname", fName);
		click("hl.file.list");
		click("hl.link.btn");
		tapHyperlink();
	}

	@QAFTestStep(description = "User tap on hyperlink")
	public static void tapHyperlink() {
		System.out.println("Clicking on the hyperlink");
		TouchAction action = new TouchAction((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());
		action.press(317, 625).release().perform();

	}

	@QAFTestStep(description = "User open the hyperlinked file")
	public static void openHyperFile() {
		System.out.println("Start file open");
		waitForPresent("hl.hyper.file");
		// click("hl.hyper.file");
		QAFExtendedWebElement ele = new QAFExtendedWebElement("hl.hyper.file");
		Point point = ele.getLocation();
		int x = point.getX() + (ele.getSize().width / 2);
		int y = (point.getY() + (ele.getSize().height) / 2);
		TouchAction action = new TouchAction((MobileDriver) new WebDriverTestBase().getDriver().getUnderLayingDriver());

		if (new QAFExtendedWebElement("hl.image.download.btn").isPresent()) {
			action.press(x, y).release().perform();
			// QAFTestBase.pause(10000);
			waitForNotPresent("hl.image.download.btn", 60);
			waitForNotPresent("hl.image.download.loader", 180);
			QAFTestBase.pause(5000);
			// action.press(x, y).release().perform();
			// QAFTestBase.pause(5000);
		}

		action.press(x, y).release().perform();
		QAFTestBase.pause(2000);
	}

	@QAFTestStep(description = "User should see the same hyperlinked file")
	public static void openHyperlinkFile() {
		System.out.println(new QAFExtendedWebElement("hl.hyper.file.header").getAttribute("value"));
		System.out.println(ConfigurationManager.getBundle().getString("fname"));
		Validator.verifyThat(new QAFExtendedWebElement("hl.hyper.file.header").getAttribute("value")
				.contains(ConfigurationManager.getBundle().getString("fname")), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "User opens the saved hyperlink")
	public static void openSavedHyperlink() {
		click("project.mrkupanotation.btn");
		click("hl.hyperlink.btn");
		QAFTestBase.pause(2000);
	}

	@QAFTestStep(description = "User should see the same hyperlinked folder")
	public static void verifyHyerlinFOlder() {
		System.out.println(ConfigurationManager.getBundle().getString("hlMultiPage"));
		System.out.println(new QAFExtendedWebElement("hl.folder.brdcrmb").getAttribute("label"));
		Validator.verifyThat(new QAFExtendedWebElement("hl.folder.brdcrmb").getAttribute("label")
				.contains(ConfigurationManager.getBundle().getString("hlMultiPage")), Matchers.equalTo(true));
	}

}
