# SkySite_Project mobile Automation

This is automation project contains all the test cases related to the SKySIte Project mobile Automation


This is a project with Maven directory structure:
 
The 'config' directory contains testng.xml file, and is a place holder for configuration files.

The 'resources' directory contains all required resources including properties files and data files, and is a place holder for other resources.

The 'src' directory contains all java files and is a place holder for other java files.

The 'test-results' directory contains result files.

The 'scenarios' directory is the default place holder for all the scenario files. 


To change/modify dependencies check pom.xml
To run the project, from command prompt go to project home and run mvn. Open dashboard.htm to view results.

Note: This sample project uses Appium driver and it requires io.appium binary.
You need to include the appium dependency in the pom.xml to include and use the appium library. 


Thanks,
Automation QA Team.
